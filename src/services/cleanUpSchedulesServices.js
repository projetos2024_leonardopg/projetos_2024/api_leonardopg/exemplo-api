const connect = require("../db/connect");

async function cleanUpSchedules(){
    const currentDate = new Date();

    //definir a data para 7 dias atras
    currentDate.setDate(currentDate.getDate() - 7);
    //formata a data para YYYY-MM-DD
    const formattedDate = currentDate.toISOString().split('T')[0];

    const query = `DELETE FROM schedule WHERE dateEnd < ?`
    const values = [formattedDate];

    return new Promise((resolve, reject)=>{
        connect.query(query,values, function(err, results){
            if(err){
                console.error("Erro Mysql", err);
                return reject(new Error("Erro ao limpar agendamento"));
            }
            console.log("Agendamentos antigos apagados");
            resolve("Agendamento antigos limpos com sucesso")
        })
    })

};
module.exports = cleanUpSchedules;