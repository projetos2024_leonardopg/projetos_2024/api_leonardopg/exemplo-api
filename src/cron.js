const cron = require("node-cron");
const cleanUpSchedules = require("./services/cleanUpSchedulesServices");

// Agendamento da limpeza

cron.schedule("*/30 * * * * *", async()=>{
    try{
        await cleanUpSchedules();
        console.log("Limpeza automatica execultada");
    }catch (error){
        console.error("Erro ao execultar limpeza automatica", error);
    }
});
